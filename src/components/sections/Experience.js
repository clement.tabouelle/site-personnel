import React from "react";

import Timeline from "../elements/Timeline";

function Experience() {
  return (
    <section className="section" id="experience">
      <div className="container">
        <h1 className="title">Parcours pro et académique</h1>
        <Timeline />
      </div>
    </section>
  );
}

export default Experience;
