import React from "react";
import AboutMe from "../sections/AboutMe";
import Skills from "../sections/Skills";
import Experience from "../sections/Experience";
import Projects from "../sections/Projects";

function Content() {
  return (
    <main>
      <AboutMe />
      <Projects />
      <Skills />
      <Experience />
    </main>
  );
}

export default Content;
