# Personal website

This is my personal website built using ReactJS and Bulma as the CSS framework.

The personal information on the website is populated from a json file that follows the [JSON Resume](https://jsonresume.org/) open source standard.

Use Node v19.0.1

## License
MIT
